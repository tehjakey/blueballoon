//
//  Grass.swift
//  blueballoon
//
//  Created by Aaron Campbell on 1/4/17.
//  Copyright © 2017 Aaron Campbell. All rights reserved.
//

import SpriteKit

class Mountain:SKSpriteNode {
    private let animationName = "MountainHit"
    // MARK: - Init
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(texture: SKTexture?, color: UIColor, size: CGSize) {
        super.init(texture: texture, color: color, size: size)
    }
    
    convenience init() {
        let texture = Textures.sharedInstance.textureWith(name: SpriteName.mountain)
        self.init(texture: texture, color: SKColor.white, size: texture.size())
        
        setup()
        setupPhysics()
        
    }
    
    // MARK: - Setup
    private func setup() {
        self.anchorPoint = CGPoint(x: 0, y: 0)
        self.position = CGPoint(x: kViewSize.width * 0.25, y: 0)
    }
    
    private func setupPhysics() {
    }
    
    // MARK: - Update
    func update(delta: TimeInterval) {
    }
    
    // MARK: - Actions
    private func animate() {
    }
    public func hit(){
        // Create the frames of the animation
        let frame0 = Textures.sharedInstance.textureWith(name: SpriteName.mountain)
        let frame1 = Textures.sharedInstance.textureWith(name: SpriteName.mountainHit)
        
        // Create the animation using the frames
        let animation = SKAction.animate(with: [frame1, frame0], timePerFrame: 0.1)
        
        // Run the animation forever
        let animationMultiply = SKAction.repeat(animation, count: 6)
        let waitForIt = SKAction.wait(forDuration: 1)
        let sequence = SKAction.sequence([waitForIt, animationMultiply])
        self.run(sequence)
        
        
    }
    public func getPeak() -> CGPoint{
        let xVal = self.position.x + self.size.width / 2
        let yVal = self.position.y + self.size.height
        return CGPoint(x: xVal, y: yVal)
    }
}
