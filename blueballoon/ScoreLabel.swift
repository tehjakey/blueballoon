//
//  ScoreLabel.swift
//  blueballoon
//
//  Created by Aaron Campbell on 1/2/17.
//  Copyright © 2017 Aaron Campbell. All rights reserved.
//

import SpriteKit

class ScoreLabel:SKNode {
    
    // MARK: - Private class constants
    private let label = SKLabelNode(fontNamed: kFont)
    
    // MARK: - Init
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init() {
        super.init()
        
        setup()
    }
    
    // MARK: - Setup
    private func setup() {
        self.position.x = kViewSize.width / 2
        self.position.y = kViewSize.height * 0.7
        
        label.text = String(0)
        label.fontColor = Colors.colorFrom(rgb: Colors.score)
        label.fontSize = 200.0
        label.alpha = 0.5
        
        self.addChild(label)
    }
    
    // MARK: - Update
    func updateScore(score: Int) {
        label.text = String(score)
    }
}
