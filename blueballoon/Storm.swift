//
//  Storm.swift
//  blueballoon
//
//  Created by Aaron Campbell on 1/11/17.
//  Copyright © 2017 Aaron Campbell. All rights reserved.
//
import SpriteKit

class Storm:SKSpriteNode {
    private let animationName = "Thunder"
    private var isHit = false
    // MARK: - Init
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(texture: SKTexture?, color: UIColor, size: CGSize) {
        super.init(texture: texture, color: color, size: size)
    }
    
    convenience init() {
        let texture = Textures.sharedInstance.textureWith(name: SpriteName.cloudDark00)
        self.init(texture: texture, color: SKColor.white, size: texture.size())
        setup()
        setupPhysics()
        
    }
    
    // MARK: - Setup
    private func setup() {
        let startX = kViewSize.width + self.size.width
        let startY = RandomFloatBetween(min: kViewSize.height * 0.8, max: kViewSize.height * 0.9)
        self.position = CGPoint(x: startX, y: startY)
    }
    
    private func setupPhysics() {
        self.physicsBody = SKPhysicsBody(rectangleOf: CGSize(width: self.size.width,
                                                             height: self.size.height))
        self.physicsBody?.affectedByGravity = false
        self.physicsBody?.categoryBitMask = Contact.storm
        self.physicsBody?.collisionBitMask = 0x0
        self.physicsBody?.contactTestBitMask = 0x0
    }
    
    // MARK: - Update
    func update(delta: TimeInterval) {
        if(isHit == false){
            let moveAmount = CGFloat(delta) * 60 * 2
            
            self.position.x = self.position.x - moveAmount
            
            if self.position.x < (0 - self.size.width ) {
                self.removeFromParent()
            }
        }
        
    }
    
    // MARK: - Actions
    public func registerHit() {
        self.physicsBody?.collisionBitMask = 0x0
        self.physicsBody?.categoryBitMask = 0x0
        if isHit == false{
            let frame0 = Textures.sharedInstance.textureWith(name: SpriteName.cloudDark01)
            let frame1 = Textures.sharedInstance.textureWith(name: SpriteName.cloudDark00)
            
        
            // Create the animation using the frames
            self.run(Sound.sharedInstance.playSound(sound: .thunder))
            let animation = SKAction.animate(with: [frame0, frame1 ], timePerFrame: 0.1)
            let thunder = SKAction.repeat(animation, count: 7)
            let resetHit = SKAction.run {
                self.isHit = false
            }
            //let completion = SKAction.removeFromParent()
            // Run the animation forever
            let sequence = SKAction.sequence([thunder, resetHit])
            self.run(sequence)
        }
        isHit = true
        
    }
}
