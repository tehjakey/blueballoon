//
//  Constants.swift
//  blueballoon
//
//  Created by Aaron Campbell on 12/26/16.
//  Copyright © 2016 Aaron Campbell. All rights reserved.
//

import SpriteKit
let kDebug = false
let kViewSize = UIScreen.main.bounds.size
let kScreenCenter = CGPoint(x: kViewSize.width / 2, y: kViewSize.height / 2)
let kFont = "Edit Undo BRK"
