//
//  Owl.swift
//  blueballoon
//
//  Created by Aaron Campbell on 1/6/17.
//  Copyright © 2017 Aaron Campbell. All rights reserved.
//

import SpriteKit

class Owl:SKSpriteNode {
    
    enum OwlSize {
        case xlarge, large, medium, small, xsmall
    }
    
    private let animationName = "OwlFly"
    private var isHit = false
    
    // MARK: - Init
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(texture: SKTexture?, color: UIColor, size: CGSize) {
        super.init(texture: texture, color: color, size: size)
    }
    
    convenience init(owlSize: OwlSize) {
        let texture = Textures.sharedInstance.textureWith(name: SpriteName.owl)
        self.init(texture: texture, color: SKColor.white, size: texture.size())
        
        setup(owlSize: owlSize)
        setupPhysics()
        
    }
    
    // MARK: - Setup
    private func setup(owlSize: OwlSize) {
        
        switch owlSize {
        case .xlarge:
            self.setScale(0.65)
            
        case .large:
            self.setScale(0.55)
            
        case .medium:
            self.setScale(0.40)
            
        case .small:
            self.setScale(0.30)
        
        case .xsmall:
            self.setScale(0.20)
            
        }
    }
    
    private func setupPhysics() {
        self.physicsBody = SKPhysicsBody(rectangleOf: CGSize(width: self.size.width / 4,
                                                             height: self.size.height))
        self.physicsBody?.affectedByGravity = false
        self.physicsBody?.categoryBitMask = Contact.owl
        self.physicsBody?.collisionBitMask = 0x0
        self.physicsBody?.contactTestBitMask = 0x0
    }
    
    // MARK: - Update
    func update(delta: TimeInterval) {
        let moveAmount = CGFloat(delta) * 60 * 2
        
        self.position.x = self.position.x - moveAmount
        
        if self.position.x < (0 - self.size.width ) {
            self.removeFromParent()
        }
        
        if isHit && self.position.x > kViewSize.width {
            self.removeFromParent()
        }
    }
    
    // MARK: - Actions
    public func registerHit() {
        isHit = true
        
        self.physicsBody?.velocity = CGVector(dx: 350.0, dy: 175.0)
        // Create the frames of the animation
        let frame0 = Textures.sharedInstance.textureWith(name: SpriteName.owlFly0)
        let frame1 = Textures.sharedInstance.textureWith(name: SpriteName.owlFly1)
        
        // Create the animation using the frames
        let animation = SKAction.animate(with: [frame1, frame0], timePerFrame: 0.1)
        
        // Run the animation forever
        self.run(SKAction.repeat(animation, count: 5), withKey: animationName)
        
    }
}
