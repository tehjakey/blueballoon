//
//  Yodeler.swift
//  blueballoon
//
//  Created by Aaron Campbell on 1/7/17.
//  Copyright © 2017 Aaron Campbell. All rights reserved.
//

import SpriteKit

class Yodeler:SKSpriteNode {
    private let refMountain = Mountain()
    private let refHills = Hills()
    private var moveSpeed:CGFloat = 0.0
    private var canMove = false
    private var isReward = false
    private var deltaCounter = 0
    private var didFall = false
    
    // MARK: - Init
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(texture: SKTexture?, color: UIColor, size: CGSize) {
        super.init(texture: texture, color: color, size: size)
    }
    
    convenience init() {
        let texture = Textures.sharedInstance.textureWith(name: SpriteName.yodeler)
        self.init(texture: texture, color: SKColor.white, size: texture.size())
        setup()
        setupPhysics()
    }
    
    // MARK: - Setup
    private func setup() {
        self.anchorPoint = CGPoint(x: 0.5, y: 0)
        self.position = CGPoint(x: refMountain.position.x + refMountain.size.width * 0.13, y: refHills.size.height * 0.6)
    }
    
    private func setupPhysics() {
    }
    
    // MARK: - Update
    func update(delta: TimeInterval) {
        if !self.isReward {
            deltaCounter += 1
        }
        
        
        if deltaCounter == 120 && isFrozen(){
            unfreeze()
        }
        if self.canMove {
            self.position.x = self.position.x + 0.15
            self.position.y = self.position.y + 0.3
        }
        
        // Remove the cloud if completely offscreen left
        if self.position.y > refMountain.size.height {
            gameOver()
        }
        // Remove when falls offscreen
        if self.position.y < 0 {
            self.removeFromParent()
        }
    }
    
    // MARK: - Actions
    public func gameOver() {
        if didFall == false {
            self.physicsBody = SKPhysicsBody(rectangleOf: CGSize(width: self.size.width / 8,
                                                                         height: self.size.height / 2))
            //So we don't collide with other objects
            self.physicsBody?.collisionBitMask =  Contact.tree & Contact.owl
            self.physicsBody?.contactTestBitMask = Contact.tree & Contact.owl
            self.physicsBody?.affectedByGravity = true
            self.physicsBody?.angularVelocity = -2.0
            self.physicsBody?.angularDamping = 0.25
            self.run(Sound.sharedInstance.playSound(sound: .yodelCrash))
            didFall = true
        }
        
    }
    
    public func freeze(){
        self.canMove = false
        self.deltaCounter = 0
        Sound.sharedInstance.pauseMusic()
    }
    
    public func unfreeze(){
        self.canMove = true
        deltaCounter = 0
        Sound.sharedInstance.playMusic()

    }
    
    public func reward() {
        self.isReward = true
        freeze()
        //move buster down the ladder -30 or back to the original starting y whichever is less
        if self.position.y - refHills.size.height > 0{
            let yDelta = -CGFloat(self.size.height / 2)
            let xDelta = yDelta / 2
            
            let moveDownRuler = SKAction.moveBy(x: xDelta, y: yDelta, duration:1.0)
            let waitForIt = SKAction.wait(forDuration: 1)
            let sequence = SKAction.sequence([waitForIt, moveDownRuler])
            self.run(sequence)
        }
        freeze()
        self.isReward = false
    }
    public func isFrozen() -> Bool {
        return !self.canMove
    }
}
