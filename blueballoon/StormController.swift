//
//  StormController.swift
//  blueballoon
//
//  Created by Aaron Campbell on 1/12/17.
//  Copyright © 2017 Aaron Campbell. All rights reserved.
//


import SpriteKit

class StormController:SKNode {
    private var frameCount:TimeInterval = 0.0
    
    // MARK: - Init
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init() {
        super.init()
        setup()
    }
    
    // MARK: - Setup
    private func setup() {
        
    }
    
    // MARK: - Spawn
    public func spawn() {
            let storm = Storm()
            let bolt = Bolt()
            bolt.position.x = storm.position.x
            bolt.position.y = storm.position.y
            self.addChild(storm)
            self.addChild(bolt)
    }
    
    // MARK: - Update
    func update(delta: TimeInterval) {
        frameCount += delta
        
        if frameCount >= 1.0 {
            frameCount = 0.0
        }
        
        for node in self.children {
            if let storm = node as? Storm {
                storm.update(delta: delta)
            }
            if let bolt = node as? Bolt {
                bolt.update(delta: delta)
            }
        }
    }
}
