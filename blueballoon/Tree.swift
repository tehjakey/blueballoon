//
//  Tree.swift
//  blueballoon
//
//  Created by Aaron Campbell on 1/2/17.
//  Copyright © 2017 Aaron Campbell. All rights reserved.
//

import SpriteKit

class Tree:SKSpriteNode {
    
    enum TreeSize {
        case xlarge, large, medium, small, xsmall
    }
    
    // MARK: - Init
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(texture: SKTexture?, color: UIColor, size: CGSize) {
        super.init(texture: texture, color: color, size: size)
    }
    
    convenience init(treeSize: TreeSize) {
        var texture = SKTexture()
        texture = Textures.sharedInstance.textureWith(name: SpriteName.tree)
        
        self.init(texture: texture, color: SKColor.white, size: texture.size())
        
        setup(treeSize: treeSize)
        setupPhysics()
        
    }
    
    // MARK: - Setup
    private func setup(treeSize: TreeSize) {
        
        switch treeSize {
        case .xlarge:
            self.setScale(1.40)
            
        case .large:
            return
            
        case .medium:
            self.setScale(0.80)
            
        case .small:
            self.setScale(0.40)
        
        case .xsmall:
            self.setScale(0.20)
            
        }
    }
    
    private func setupPhysics() {
        //self.physicsBody = SKPhysicsBody(texture: self.texture!, size: self.size)
        self.physicsBody = SKPhysicsBody(rectangleOf: CGSize(width: self.size.width / 10,
                                                             height: self.size.height * 0.9))
        self.physicsBody?.affectedByGravity = false
        self.physicsBody?.categoryBitMask = Contact.tree
        self.physicsBody?.collisionBitMask = 0x0
        self.physicsBody?.contactTestBitMask = 0x0
    }
    
    // MARK: - Update
    func update(delta: TimeInterval) {
        let moveAmount = CGFloat(delta) * 60 * 2
        
        self.position.x = self.position.x - moveAmount
        
        if self.position.x < (0 - self.size.width) {
            self.removeFromParent()
        }
    }
    
    // MARK: - Actions
    private func animate() {
    }
}
