//
//  LevelController.swift
//  blueballoon
//
//  Created by Aaron Campbell on 1/8/17.
//  Copyright © 2017 Aaron Campbell. All rights reserved.
//

import SpriteKit

class LevelController:SKNode {
    
    // MARK: - Private class constants
    private var markerArray = [LevelMarker]()
    private let levelCompleteScoreCount = 10
    private var frameCount:TimeInterval = 0.0
    private var numberOfLevels = 3
    private var currentLevel = 0
    
    
    private let level1 = LevelMarker()
    private let level2 = LevelMarker()
    private let level3 = LevelMarker()
    // MARK: - Init
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init() {
        super.init()
        setup()
    }
    
    // MARK: - Setup
    private func setup() {
        markerArray = [ level1, level2, level3]
        // Child sprite that will be added to the right
        level2.anchorPoint = CGPoint.zero
        level2.position = CGPoint(x: level1.size.width, y: kViewSize.height - level2.size.height)
        level3.anchorPoint = CGPoint.zero
        level3.position = CGPoint(x: level1.size.width * 2, y: kViewSize.height - level3.size.height)
        
        self.addChild(level1)
        self.addChild(level2)
        self.addChild(level3)
    }

    // MARK: - Update
    func update(delta: TimeInterval) {

    }
    
    private func increaseDifficulty()-> Bool{
        if currentLevel <= numberOfLevels {
            //markerArray[self.currentLevel - 1].complete()
            self.currentLevel += 1
            self.run(Sound.sharedInstance.playSound(sound: .highScore))
            return true
        }
        return false
    }
    
    public func getCurrentLevel() -> Int {
        return currentLevel
    }
    
    public func checkForLevelChange(score: Int) -> Bool {
        if 0 == (score % levelCompleteScoreCount){
                return increaseDifficulty()
        }
        return false
        
    }
}
