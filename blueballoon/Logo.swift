//
//  Logo.swift
//  blueballoon
//
//  Created by Aaron Campbell on 12/29/16.
//  Copyright © 2016 Aaron Campbell. All rights reserved.
//

import SpriteKit

class Logo:SKSpriteNode {
    
    // MARK: - Private class constants
    private let burn0 = Textures.sharedInstance.textureWith(name: SpriteName.logo0)
    private let burn1 = Textures.sharedInstance.textureWith(name: SpriteName.logo1)
    private let burnTime:TimeInterval = 0.4
    
    // MARK: - Init
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(texture: SKTexture?, color: UIColor, size: CGSize) {
        super.init(texture: texture, color: color, size: size)
    }
    
    convenience init() {
        let texture = Textures.sharedInstance.textureWith(name: SpriteName.logo0)
        self.init(texture: texture, color: SKColor.white, size: texture.size())
        
        setup()
        setupPhysics()
        animate()
    }
    
    // MARK: - Setup
    private func setup() {
        self.position.x = kViewSize.width * 0.5
        self.position.y = kViewSize.height * 0.65
    }
    
    private func setupPhysics() {
    }
    
    // MARK: - Update
    func update(delta: TimeInterval) {
    }
    
    // MARK: - Actions
    private func animate() {
        let frames = [burn0, burn1]
        
        let animation = SKAction.animate(with: frames, timePerFrame: burnTime)
        
        self.run(SKAction.repeatForever(animation))
    }
}
