//
//  LevelCounter.swift
//  blueballoon
//
//  Created by Aaron Campbell on 1/8/17.
//  Copyright © 2017 Aaron Campbell. All rights reserved.
//

import SpriteKit

class LevelMarker:SKSpriteNode {
    
    // MARK: - Private class constants
    private let levelOnTexture = Textures.sharedInstance.textureWith(name: SpriteName.levelOn)
    
    // MARK: - Private class variables
    private var gamePaused = false
    
    // MARK: - Init
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(texture: SKTexture?, color: UIColor, size: CGSize) {
        super.init(texture: texture, color: color, size: size)
    }
    
    convenience init() {
        let texture = Textures.sharedInstance.textureWith(name: SpriteName.levelOff)
        self.init(texture: texture, color: SKColor.white, size: texture.size())
        
        setup()
    }
    
    // MARK: - Setup
    private func setup() {
        // Put the anchorPoint in the top right corner of the sprite
        self.anchorPoint = CGPoint(x: 0.0, y: 0.0)
        
        // Position at top right corner of screen
        self.position = CGPoint(x: 0, y: kViewSize.height - self.size.height)
        self.isHidden = true
        
    }
    
    // MARK: - Actions
    func complete() {
        self.isHidden = false
    }
}
