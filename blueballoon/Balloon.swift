//
//  Floppy.swift
//  blueballoon
//
//  Created by Aaron Campbell on 12/31/16.
//  Copyright © 2016 Aaron Campbell. All rights reserved.
//

import SpriteKit

class Balloon:SKSpriteNode {
    
    
    // MARK: - Private class constants
    private let animationName = "Burner"
    
    // MARK: - Private class variables
    private var score = 0
    
    // MARK: - Init
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(texture: SKTexture?, color: UIColor, size: CGSize) {
        super.init(texture: texture, color: color, size: size)
    }
    
    convenience init() {
        let texture = Textures.sharedInstance.textureWith(name: SpriteName.player0)
        self.init(texture: texture, color: SKColor.white, size: texture.size())
        
        setup()
        setupPhysics()
        
    }
    
    // MARK: - Setup
    private func setup() {
        self.position = CGPoint(x: kViewSize.width * 0.3, y: kViewSize.height * 0.8)
    }
    
    private func setupPhysics() {
        self.physicsBody = SKPhysicsBody(rectangleOf: CGSize(width: self.size.width / 4,
                                          height: self.size.height))
        self.physicsBody?.categoryBitMask = Contact.balloon
        self.physicsBody?.collisionBitMask = Contact.scene
        self.physicsBody?.contactTestBitMask = Contact.scene | Contact.tree | Contact.score | Contact.owl | Contact.storm | Contact.bolt
    }
    
    // MARK: - Update
    func update() {

    }
    
    // MARK: - Actions
    func fly () {
        self.physicsBody!.velocity = CGVector.zero
        
        let impulse = CGVector(dx: 0, dy: self.size.height * 0.2)
        
        self.physicsBody!.applyImpulse(impulse)
        // Create the frames of the animation
        let frame0 = Textures.sharedInstance.textureWith(name: SpriteName.player0)
        let frame1 = Textures.sharedInstance.textureWith(name: SpriteName.player1)
        
        // Create the animation using the frames
        let animation = SKAction.animate(with: [frame1, frame0], timePerFrame: 0.5)
        
        // Run the animation forever
        self.run(SKAction.repeat(animation, count: 1), withKey: animationName)
        self.run(Sound.sharedInstance.playSound(sound: .flap))
    }
    
    // MARK: - Update score
    func updateScore() {
        score += 1
        self.run(Sound.sharedInstance.playSound(sound: .score))
    }
    
    func getScore() -> Int {
        return score
    }
    
    func crashed() {
        self.run(Sound.sharedInstance.playSound(sound: .crash))
    }
    
    func smacked() {
        // Play the smack sound followed by the fall sound
        self.run(Sound.sharedInstance.playSound(sound: .crash))
    }
    
    private func stopAnimation() {
        self.removeAction(forKey: animationName)
    }
    
    private func animateSmoke() {
        let smoke = Smoke()
        self.addChild(smoke)
        smoke.animate()
    }

    
    // MARK: - Actions
    private func animate() {
    }
    
    // MARK: - Game Over
    func gameOver() {
        stopAnimation()
        animateSmoke()
        checkScore()
    }
    
    private func checkScore() {
        if score > Settings.sharedInstance.getBestScore() {
            self.run(Sound.sharedInstance.playSound(sound: .highScore))
            Settings.sharedInstance.saveBest(score: score)
        }
    }
}
