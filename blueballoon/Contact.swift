//
//  Contact.swift
//  blueballoon
//
//  Created by Aaron Campbell on 12/31/16.
//  Copyright © 2016 Aaron Campbell. All rights reserved.
//

import Foundation

class Contact {
    class var scene:UInt32      { return 1 << 0 }
    class var tree:UInt32       { return 1 << 1 }
    class var balloon:UInt32    { return 1 << 2 }
    class var score:UInt32      { return 1 << 3 }
    class var owl:UInt32        { return 1 << 4 }
    class var storm:UInt32      { return 1 << 5 }
    class var bolt:UInt32       { return 1 << 6 }
}
