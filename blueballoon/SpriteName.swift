//
//  SpriteName.swift
//  blueballoon
//
//  Created by Aaron Campbell on 12/29/16.
//  Copyright © 2016 Aaron Campbell. All rights reserved.
//

import Foundation

class SpriteName {
    
    // Button Sprite Names
    class var playButton:String         { return "PlayButton" }
    class var retryButton:String        { return "RetryButton" }
    class var tutorial:String           { return "Tutorial" }
    class var pauseButton:String        { return "PauseButton" }
    class var resumeButton:String       { return "ResumeButton" }
    
    // Interface Sprite Names
    class var title:String              { return "GameTitle" }
    class var gameOver:String           { return "GameOver" }
    class var logo0:String              { return "Balloon000" }
    class var logo1:String              { return "Balloon001" }
    
    // Scoreboard
    class var scoreboard:String         { return "Scoreboard" }
    
    // Environment Sprite Names
    class var ground:String             { return "Ground" }
    class var cloud:String              { return "Cloud" }
    class var cloudDark00:String        { return "CloudDark00" }
    class var cloudDark01:String        { return "CloudDark01" }
    class var bolt00:String             { return "Bolt00" }
    class var bolt01:String             { return "Bolt01" }
    class var hills:String              { return "Hills" }
    class var mountain:String           { return "MountainRuler" }
    class var mountainHit:String        { return "MountainRulerHit" }
    class var owl:String                { return "Owl" }
    class var owlFly0:String            { return "OwlFly0" }
    class var owlFly1:String            { return "OwlFly1" }
    class var yodeler:String            { return "Yodeler" }
    class var levelOff:String           { return "LevelOff" }
    class var levelOn:String            { return "LevelOn" }
    class var blueOx:String             { return "BlueOx" }
    class var blueOxHit:String             { return "BlueOxHit" }
    
    // Character Sprite Names
    class var player0:String            { return "Balloon000" }
    class var player1:String            { return "Balloon001" }
    class var player2:String            { return "BalloonCrash" }
    
    
    // Obstacle Sprite Names
    class var tree:String          { return "Tree" }
}
