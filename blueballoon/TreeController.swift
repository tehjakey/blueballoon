//
//  TreeController.swift
//  blueballoon
//
//  Created by Aaron Campbell on 1/2/17.
//  Copyright © 2017 Aaron Campbell. All rights reserved.
//

import SpriteKit

class TreeController:SKNode {
    
    // MARK: - Private class constants
    private let refGround = Ground()
    
    private let xlTree = Tree(treeSize: .xlarge)
    private let largeTree = Tree(treeSize: .large)
    private let mediumTree = Tree(treeSize: .medium)
    private let smallTree = Tree(treeSize: .small)
    private let xsmallTree = Tree(treeSize: .xsmall)
    
    private let xlOwl = Owl(owlSize: .xlarge)
    private let largeOwl = Owl(owlSize: .large)
    private let mediumOwl = Owl(owlSize: .medium)
    private let smallOwl = Owl(owlSize: .small)
    private let xsmallOwl = Owl(owlSize: .xsmall)
    
    // MARK: - Private class variables
    private var treeArray1 = [Tree]()
    private var owlArray1 = [Owl]()
    
    private var treeArray2 = [Tree]()
    private var owlArray2 = [Owl]()
    
    private var treeArray3 = [Tree]()
    private var owlArray3 = [Owl]()
    
    private var treeArray = [Tree]()
    private var owlArray = [Owl]()
    
    private var frameCount:TimeInterval = 0.0
    private var level = 0
    private var numberOfLevels = 3

    private var runRate = 2.0
    // MARK: - Init
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init() {
        super.init()
        setup()
    }
    
    // MARK: - Setup
    private func setup() {
        treeArray = [ largeTree, xlTree ]
        owlArray = [ largeOwl, xlOwl ]
        treeArray1 = [ mediumTree, largeTree,mediumTree, xlTree, mediumTree, largeTree]
        owlArray1 = [ mediumOwl, largeOwl,mediumOwl, xlOwl, mediumOwl, largeOwl ]
        treeArray2 = [ smallTree, largeTree,smallTree, largeTree,smallTree, largeTree ]
        owlArray2 = [ smallOwl, largeOwl,smallOwl, largeOwl,smallOwl, largeOwl, ]
        treeArray3 = [ xsmallTree, mediumTree,xsmallTree, mediumTree,xsmallTree, mediumTree, xlTree ]
        owlArray3 = [ xsmallOwl, mediumOwl,xsmallOwl, mediumOwl,xsmallOwl, mediumOwl, xlOwl ]
    }
    
    // MARK: - Spawn
    private func spawn() {
        
        let randomIndex = RandomIntegerBetween(min: 0, max: treeArray.count - 1 )
        
        
        let newTree = treeArray[randomIndex].copy() as! Tree
        let newOwl = owlArray[randomIndex].copy() as! Owl
        
        newTree.position = CGPoint(x: kViewSize.width + largeTree.size.width, y: (newTree.size.height / 2) + refGround.size.height * 0.75)
    
        newOwl.position = CGPoint(x: kViewSize.width + largeTree.size.width, y: newTree.size.height + refGround.size.height * 0.75 + (newOwl.size.height / 2) * 0.85)
    
        self.addChild(newTree)
        self.addChild(newOwl)
    }
    
    // MARK: - Update
    func update(delta: TimeInterval) {
        frameCount += delta
        
        if frameCount >= self.runRate {
            spawn()
            frameCount = 0.0
        }
        
        for node in self.children {
            if let tree = node as? Tree {
                tree.update(delta: delta)
            }
            if let owl = node as? Owl {
                owl.update(delta: delta)
            }
        }
    }
    public func setLevel(level: Int){
        if level < numberOfLevels {
            self.level = level
            if level == 1 {
                treeArray = treeArray1
                owlArray = owlArray1
            } else if level == 2 {
                treeArray = treeArray2
                owlArray = owlArray2
                self.runRate = 1.5
            } else if level == 3 {
                treeArray = treeArray3
                owlArray = owlArray3
            }
        }
    }
}
