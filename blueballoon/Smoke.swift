//
//  Smoke.swift
//  blueballoon
//
//  Created by Aaron Campbell on 1/5/17.
//  Copyright © 2017 Aaron Campbell. All rights reserved.
//

import SpriteKit

class Smoke:SKSpriteNode {
    
    // MARK: - Private class variables
    private var animation = SKAction()
    
    // MARK: - Init
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(texture: SKTexture?, color: UIColor, size: CGSize) {
        super.init(texture: texture, color: color, size: size)
    }
    
    convenience init() {
        let texture = Textures.sharedInstance.textureWith(name: SpriteName.player2)
        self.init(texture: texture, color: SKColor.white, size: texture.size())
        
        setup()
    }
    
    // MARK: - Setup
    private func setup() {
        let frame0 = Textures.sharedInstance.textureWith(name: SpriteName.player2)
        let frame1 = Textures.sharedInstance.textureWith(name: SpriteName.player0)
        let frame2 = Textures.sharedInstance.textureWith(name: SpriteName.player2)
        let frame3 = Textures.sharedInstance.textureWith(name: SpriteName.player0)
        let frame4 = Textures.sharedInstance.textureWith(name: SpriteName.player2)
        
        animation = SKAction.animate(with: [frame0, frame1, frame2, frame3, frame4], timePerFrame: 0.23)
        
        self.alpha = 0.0
    }
    
    // MARK: - Actions
    func animate() {
        self.alpha = 1.0
        
        self.run(self.animation, completion:  {
            [weak self] in
            self?.removeFromParent()
        })
    }
}
