//
//  Bolt.swift
//  blueballoon
//
//  Created by Aaron Campbell on 1/11/17.
//  Copyright © 2017 Aaron Campbell. All rights reserved.
//

import SpriteKit

class Bolt:SKSpriteNode {
    private var isHit = false
    private let refMountain = Mountain()
    // MARK: - Init
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(texture: SKTexture?, color: UIColor, size: CGSize) {
        super.init(texture: texture, color: color, size: size)
    }
    
    convenience init() {
        let texture = Textures.sharedInstance.textureWith(name: SpriteName.bolt00)
        self.init(texture: texture, color: SKColor.white, size: texture.size())
        setup()
        setupPhysics()
        
    }
    
    // MARK: - Setup
    private func setup() {
        let startX = kViewSize.width + self.size.width
        let startY = RandomFloatBetween(min: kViewSize.height, max: kViewSize.height)
        self.position = CGPoint(x: startX, y: startY)        
    }
    
    private func setupPhysics() {
        self.physicsBody = SKPhysicsBody(rectangleOf: CGSize(width: self.size.width,
                                                             height: self.size.height))
        self.physicsBody?.affectedByGravity = false
        self.physicsBody?.categoryBitMask = Contact.bolt
        self.physicsBody?.collisionBitMask = 0x0
        self.physicsBody?.contactTestBitMask = 0x0
    }
    
    // MARK: - Update
    func update(delta: TimeInterval) {
        if isHit == false{
            let moveAmount = CGFloat(delta) * 60 * 2
            
            self.position.x = self.position.x - moveAmount
            
            if self.position.x < (0 - self.size.width ) {
                self.removeFromParent()
            }
        }
    }
    
    // MARK: - Actions
    public func registerHit(moveTo: CGPoint) {
        self.physicsBody?.collisionBitMask = 0x0
        self.physicsBody?.categoryBitMask = 0x0
        if isHit == false{
            isHit = true
            let frame0 = Textures.sharedInstance.textureWith(name: SpriteName.bolt01)
            let frame1 = Textures.sharedInstance.textureWith(name: SpriteName.bolt00)
            let cloudPos = self.position
            let moveToCloud = SKAction.move(to: cloudPos, duration:0.1)
            let moveToYodeler = SKAction.move(to: moveTo, duration:0.1)
            let rotate = SKAction.rotate(toAngle: CGFloat(M_PI/3.0), duration: 0.0)
            
            // Create the animation using the frames
            let animation = SKAction.animate(with: [frame1, frame0, frame1], timePerFrame: 0.15)
            let thunder = SKAction.repeat(animation, count: 2)
            let sound = Sound.sharedInstance.playSound(sound: .highScore)
            let sequence = SKAction.sequence([rotate,thunder, moveToYodeler,sound, moveToCloud, moveToYodeler, sound, moveToCloud,moveToYodeler, sound, SKAction.removeFromParent()])
            self.run(sequence)
        }
        
    }
}
