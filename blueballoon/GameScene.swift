//
//  GameScene.swift
//  blueballoon
//
//  Created by Aaron Campbell on 12/26/16.
//  Copyright © 2016 Aaron Campbell. All rights reserved.
//

import SpriteKit
import GameplayKit


class GameScene: SKScene,  SKPhysicsContactDelegate {
    // MARK: - Private enum
    private enum State {
        case tutorial, running, paused, gameOver
    }
    
    // MARK: - Private class constants
    private let cloudController = CloudController()
    private let stormController = StormController()
    private let mountain = Mountain()
    private let yodeler = Yodeler()
    private let hills = Hills()
    private let ground = Ground()
    private let tutorial = Tutorial()
    private let balloon = Balloon()
    private let treeController = TreeController()
    private let scoreLabel = ScoreLabel()
    private let gameNode = SKNode()
    private let interfaceNode = SKNode()
    private let pauseButton = PauseButton()
    private let levelController = LevelController()

    // MARK: - Private class variables
    private var lastUpdateTime:TimeInterval = 0.0
    private var state:State = .tutorial
    private var previousState:State = .tutorial
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(size: CGSize) {
        super.init(size: size)
    }
    
    override func didMove(to view: SKView) {
        // Register notifications for "Pause"
        NotificationCenter.default.addObserver(self, selector: #selector(paused), name: NSNotification.Name(rawValue: "Pause"), object: nil)
        
        // Register notifications for "Resume"
        NotificationCenter.default.addObserver(self, selector: #selector(resumeGame), name: NSNotification.Name(rawValue: "Resume"), object: nil)
        
        setup()
    }
    
    // MARK: - Setup
    private func setup() {
        self.backgroundColor = Colors.colorFrom(rgb: Colors.sky)
        self.run(Sound.sharedInstance.playSound(sound: .owl))
        self.addChild(scoreLabel)
        self.addChild(stormController)
        self.addChild(cloudController)
        self.addChild(mountain)
        self.addChild(yodeler)
        self.addChild(tutorial)
        self.addChild(interfaceNode)
        self.addChild(levelController)
        self.addChild(hills)
        self.addChild(treeController)
        self.addChild(ground)
        self.addChild(balloon)
        
        interfaceNode.addChild(pauseButton)
        
        
        self.physicsWorld.contactDelegate = self
        self.physicsWorld.gravity = CGVector.zero
        
        self.physicsBody = SKPhysicsBody(edgeLoopFrom: self.frame)
        self.physicsBody?.affectedByGravity = false
        self.physicsBody?.categoryBitMask = Contact.scene
        self.physicsBody?.collisionBitMask = 0x0
        self.physicsBody?.contactTestBitMask = 0x0
    }
    
    // MARK: - Update
    override func update(_ currentTime: TimeInterval) {
        let delta = currentTime - lastUpdateTime
        lastUpdateTime = currentTime
        
        switch state {
        case .tutorial:
            cloudController.update(delta: delta)
            
        case .running:
            cloudController.update(delta: delta)
            stormController.update(delta: delta)
            balloon.update()
            treeController.update(delta: delta)
            ground.update(delta: delta)
            hills.update(delta: delta)
            yodeler.update(delta: delta)
            
            if yodeler.position.y < 0 {
                gameOver()
            }
            
        case .paused:
            return
            
        case .gameOver:
            return
        }
        
    }

    // MARK: - Touch Events
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch:UITouch = touches.first! as UITouch
        let touchLocation = touch.location(in: self)
        
        if pauseButton.contains(touchLocation) {
            pausePressed()
        }
        
        if state == .tutorial {
            tutorial.tappedIt()
            running()
        } else if state == .running {
            balloon.fly()
        }
    }
    
    // MARK: - Contact
    func didBegin(_ contact: SKPhysicsContact) {
        if state != .running {
            return
        } else {
            // Which body is not Balloon?
            let other = contact.bodyA.categoryBitMask == Contact.balloon ? contact.bodyB : contact.bodyA
            
            if other.categoryBitMask == Contact.scene {
                // Player hit the ground or the ceiling
                balloon.crashed()
                gameOver()
            }
            
            if other.categoryBitMask == Contact.tree {
                // Player hit a tree
                balloon.smacked()
                gameOver()
            }
            if other.categoryBitMask == Contact.storm {
                // Player hit a tree
                if let storm  = other.node as? Storm{
                    storm.registerHit()
                }
            }
            if other.categoryBitMask == Contact.bolt {
                if let bolt  = other.node as? Bolt{
                    bolt.registerHit(moveTo: mountain.getPeak())
                }
                mountain.hit()
                yodeler.reward()
            }
            if other.categoryBitMask == Contact.owl {
                balloon.updateScore()
                scoreLabel.updateScore(score: balloon.getScore())
                yodeler.freeze()
                if levelController.checkForLevelChange(score: balloon.getScore()) {
                    shake()
                    flash()
                    stormController.spawn()
                    treeController.setLevel(level: levelController.getCurrentLevel())
                }
                if let owl  = other.node as? Owl{
                    owl.registerHit()
                }   
            }
        }
    }
    // MARK: - State
    private func running() {
        state = .running
        
        self.physicsWorld.gravity = CGVector(dx: 0, dy: -2.2)
    }
    
    func paused() {
        previousState = state
        state = .paused
        
        gameNode.speed = 0.0
        
        self.physicsWorld.gravity = CGVector.zero
        self.physicsWorld.speed = 0.0
    }
    
    func resume() {
        state = previousState
        
        gameNode.speed = 1.0
        
        if previousState == .running  || previousState == .gameOver {
            self.physicsWorld.gravity = CGVector(dx: 0, dy: -2.2)
        } else {
            self.physicsWorld.gravity = CGVector.zero
        }
        
        self.physicsWorld.speed = 1.0
    }
    
    func resumeGame() {
        // Run a timer that resumes the game after 1 second
        Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(resume), userInfo: nil, repeats: false)
    }
    
    func gameOver() {
        state = .gameOver
        shake()
        flash()
        Sound.sharedInstance.pauseMusic()
        balloon.gameOver()
        yodeler.gameOver()
        
        pauseButton.run(SKAction.scale(to: 0.0, duration: 0.25), completion: {
            [weak self] in
            self?.pauseButton.removeFromParent()
        })
        
        self.run(SKAction.wait(forDuration: 2.0), completion: {
            [weak self] in
            self?.loadScene()
        })
    }
    
    private func pausePressed() {
        // Flip the texture shown on the button
        pauseButton.tapped()
        
        if pauseButton.getPauseState() {
            // Set the state to paused
            paused()
            
            // Pause the gameNode
            gameNode.isPaused = true
        } else {
            // Resume the previous state
            resume()
            
            // Resume the gameNode
            gameNode.isPaused = false
        }
    }
    
    // MARK: - Scene Animations
    private func shake() {
        gameNode.run(SKAction.shake(amount: CGPoint(x: 16, y: 16), duration: 0.5))
    }
    
    private func flash() {
        let colorFlash = SKAction.run {
            [weak self] in
            self?.backgroundColor = Colors.colorFrom(rgb: Colors.flash)
            
            self?.run(SKAction.wait(forDuration: 0.5), completion: {
                [weak self] in
                self?.backgroundColor = Colors.colorFrom(rgb: Colors.sky)
            })
        }
        
        self.run(colorFlash)
    }
    
    // MARK: - Load Scene
    private func loadScene() {
        let scene = GameOverScene(size: kViewSize, score: balloon.getScore())
        let transition = SKTransition.fade(with: SKColor.black, duration: 0.5)
        self.view?.presentScene(scene, transition: transition)
    }
    // MARK: - Deinit
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
}
