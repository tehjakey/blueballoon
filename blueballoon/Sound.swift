//
//  Sound.swift
//  blueballoon
//
//  Created by Aaron Campbell on 1/2/17.
//  Copyright © 2017 Aaron Campbell. All rights reserved.
//

import SpriteKit
import AVFoundation

enum SoundEffect {
    case crash, flap, highScore, pop, score, owl, yodel, yodelCrash, thunder, bolt, zap
}

class Sound {
    
    static let sharedInstance = Sound()
    
    // MARK: - Private class constants
    private let music = "YodelMusic.mp3"
    private let crashSound = "Crash.caf"
    private let flapSound = "Flap.caf"
    private let highScoreSound = "HighScore.caf"
    private let popSound = "Pop.caf"
    private let scoreSound = "Score.caf"
    private let owlSound = "Owl.caf"
    private let yodelSound = "Yodel.caf"
    private let yodelCrashSound = "YodelCrash.caf"
    private let thunder = "Thunder"
    private let bolt = "Bolt"
    private let zap = "Zap"
    
    // MARK: - Private class variables
    private var player = AVAudioPlayer()
    private var initialized = false
    
    // MARK: - Music player
    func playMusic() {
        let url = URL(fileURLWithPath: Bundle.main.path(forResource: music, ofType: nil)!)
        
        do {
            player = try AVAudioPlayer(contentsOf: url)
        } catch let error as NSError {
            NSLog("Error playing music: %@", error)
        }
        
        player.numberOfLoops = -1
        player.volume = 0.25
        player.prepareToPlay()
        player.play()
        
        initialized = true
    }
    
    func stopMusic() {
        if initialized && player.isPlaying {
            player.stop()
        }
    }
    
    func pauseMusic() {
        if initialized && player.isPlaying {
            player.pause()
        }
    }
    
    func resumeMusic() {
        if initialized {
            player.play()
        }
    }
    
    // MARK: - Sound Effects
    func playSound(sound: SoundEffect) -> SKAction {
        var file = String()
        
        switch sound {
        case .crash:
            file = crashSound
            
        case .flap:
            file = flapSound
            
        case .highScore:
            file = highScoreSound
            
        case .pop:
            file = popSound
            
        case .score:
            file = scoreSound
            
        case .owl:
            file = owlSound
            
        case .yodel:
            file = yodelSound
            
        case .yodelCrash:
            file = yodelCrashSound
        
        case .thunder:
            file = thunder
            
        case .bolt:
            file = bolt
            
        case .zap:
            file = zap
        }
        
        return SKAction.playSoundFileNamed(file, waitForCompletion: false)
    }
}
